import java.util.Scanner;

public class Soal08 {
    public static void main(String[] args) {
        System.out.println("Soal 8");

        Scanner userInput = new Scanner(System.in);

        System.out.println(" langkah yang akan digeser\t: ");
        int charToRatated = Integer.parseInt(userInput.nextLine());

        System.out.println("Masukkan kata \t: ");
        String userWord = userInput.nextLine().toLowerCase();

        String originalAlphabet = "abcdefghijklmnopqrstuvwxyz";
        char[] alphabetArray = originalAlphabet.toCharArray();
        int alphabetLength = alphabetArray.length;

        int charLength = userWord.length();
        String alphabetRotation = "";
        String result = "";

        int indexed;
        int increamentRotated = 0;

        for (int j = 0; j < charLength; j++) {
            char aCharValue = userWord.toCharArray()[j];
            for (int i = 0 ; i < alphabetLength -1; i++) {
                indexed = aCharValue - 'a';
                if(indexed <= 25 && indexed >= 0) {
                    if (aCharValue == alphabetArray[i]) {
                        if (charToRatated + i <= alphabetLength - 1) {
                            result += originalAlphabet.charAt(i + charToRatated);
                        } else {
                            result += originalAlphabet.charAt(j - 1);
                        }
                    }
                }
            }
        }

        // Memproses rotasi pada ke 26 huruf alphabet.
        for (int j = 0; j < originalAlphabet.length(); j++) {
            char aCharValue = originalAlphabet.toCharArray()[j];
            for (int i = 0; i < alphabetLength; i++) {
                indexed = aCharValue - 'a';
                if (indexed <= 25 && indexed >= 0) {
                    if (aCharValue == alphabetArray[i]) {
                        if (charToRatated + i <= alphabetLength - 1) {
                            alphabetRotation += originalAlphabet.charAt(i + charToRatated);
                        } else {
                            if (increamentRotated < charToRatated) {
                                alphabetRotation += originalAlphabet.charAt(increamentRotated);
                                increamentRotated++;
                            }
                        }
                    }
                }
            }
        }

        String reverseResult = "";
        for (int i = 0; i < charLength ; i++) {
            char aCharValue = userWord.toCharArray()[i];
            for (int j = 0; j < alphabetLength - 1; j++) {
                indexed = aCharValue - 'a';
                if (indexed <= 25 && indexed >= 0) {
                    reverseResult += originalAlphabet.charAt(25 - indexed);
                }
            }
        }

        System.out.println(userWord);
        System.out.println(reverseResult);
//        System.out.println(originalAlphabet);
//        System.out.println("\n" + alphabetRotation);
//        System.out.println(userWord + " [" + charLength + "] "+ "\t:\t" +result + " [" + result.length() + "] ");

    }
}
