import java.util.Scanner;

public class Soal06 {
    public static void main(String[] args) {
        System.out.println("Soal 6 : ");

        Scanner userInput = new Scanner(System.in);

        System.out.println("Masukkan banyak N nilai yang akan ditampilkan : ");
        int userNumber = userInput.nextInt();

        // Bilangan kelipatan tiga
        // 3, 6, 9, 12, 15, 18, 21

        // Kelipatan 3 yang ganjil dan tidak habis dibagi 5
        int valueByThree = 3;
        int counteNumber = 0;
//        for (int i = 0; i < (userNumber*2); i++) {
        while(true){
            if(counteNumber == userNumber){
                break;
            }
            // Bilangan kelipatan tiga yang juga bernilai genap
            if(valueByThree % 2 != 0 && valueByThree % 5 != 0){
                System.out.print(valueByThree + " ");
                counteNumber++;
            }
            valueByThree += 3;
        }
//        }
    }
}
