import java.util.Scanner;

public class Soal07 {
    public static void main(String[] args) {
        // TODO: Masih belum
        System.out.println("Soal 7 : ");

        Scanner userInput = new Scanner(System.in);

        System.out.println("Panjang tali yang diinginkan : ");
        double panjangTali = userInput.nextDouble();

        System.out.println("Target berapa panjang untuk potongan tali : ");
        double targetPotongan = userInput.nextDouble();

        double hasilPerhitungan = 0;

        int countIterasi = 0;

        // Output : berapa kali mengunting tali tersebut untuk memenuhi target potongan.

        while(hasilPerhitungan != targetPotongan) {
            double initPotongan = panjangTali / 2;
            countIterasi++;
            if (initPotongan != targetPotongan) {
                double iterasiPotongan = initPotongan / 2;
                panjangTali = iterasiPotongan;
                countIterasi++;
            } else {
                hasilPerhitungan = initPotongan;
            }
        }

        System.out.println("Perlu mengunting sebanyak " + countIterasi + " kali");
    }
}
