import java.util.Scanner;

public class Soal09 {
    public static void PrintArray(int[] intOfArray) {
        int lengtOfArray = intOfArray.length;
        for (int i = 0; i < lengtOfArray; i++) {
            System.out.print(intOfArray[i] + " ");
        }
    }

    public static void main(String[] args) {
        System.out.println("Soal 09 : ");

        Scanner userInput = new Scanner(System.in);
        System.out.println("Tentukan nilai x :\t");
        int userNumber = userInput.nextInt();
        int initNumber =  1;
        int increasedNumber = 0;
        int currentNumber = 0;

        int countResult = 0;
        int countByCondition = 0;

        while(currentNumber < userNumber){
            currentNumber = initNumber + increasedNumber;
            initNumber = increasedNumber;
            increasedNumber = currentNumber;
            countResult = currentNumber;
            System.out.print(countResult + " ");
            if (increasedNumber < userNumber && increasedNumber % 2 == 0) {
                System.out.print(countResult + " ");
                countByCondition++;
            }
        }
        System.out.println("\nSebanyak " + countByCondition);
    }
}
