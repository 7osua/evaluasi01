import java.sql.SQLOutput;
import java.util.Locale;
import java.util.Scanner;

public class Soal05 {
    public static void main(String[] args) {
        // TODO : Belum selesai
        System.out.println("Soal 05 : ");

        Scanner userInput = new Scanner(System.in);

        System.out.println("Input kata / kalimat : ");
        char[] words = userInput.nextLine().trim().toLowerCase(Locale.ROOT).toCharArray();
        int wordCharLength = words.length;

        int[] charValues = new int[wordCharLength];
        boolean[] same = new boolean[wordCharLength];

        for (int i = 0; i < wordCharLength; i++) {
            System.out.println("Nilai Ke : " + ( i + 1));
            charValues[i] = userInput.nextInt();
        }

        for (int i = 0; i < wordCharLength; i++) {
            if ((words[i] - 'a') + 1 == charValues[i]){
                same[i] = true;
            } else {
                same[i] = false;
            }
        }

        for (boolean isSame: same) {
            System.out.println(isSame);
        }

//        System.out.println((int) 'a');
//        System.out.println((int) 'z');
    }
}
