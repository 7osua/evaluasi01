import java.util.Scanner;

public class Soal01 {
    public static void main(String[] args) {
        System.out.println("Soal 01 : ");
        Scanner input = new Scanner(System.in);

        System.out.println("Input nilai n :");
        int n = input.nextInt();

        int[] arrayDivideByTwo = new int[n];
        int[] arrayThreeMinusOne = new int[n];
        int numberThreeMinusOne = 3;
        int numberFourDivideByTwo = 2;
        int arraySum[] = new int[n];

        //Bilangan 3-1
        // Kelipatan 3 : 3-1, 6-1, 9-1, 12-1
        // Angka ke kelipatan tiga yang ganjil tapi tidak habis dibagi 5 !!!!!!!!!!
        for (int i = 0; i < n ; i++) {
            if (numberThreeMinusOne % 3 == 0 && numberThreeMinusOne % 5 != 0){
                arrayThreeMinusOne[i]  = numberThreeMinusOne;
            }
            numberThreeMinusOne += 3;
            System.out.println(arrayThreeMinusOne[i]);
        }

        //Bilangan 4/2
        // Kelipatan 4/2 : 8/2, 12/2, 16/2
        for (int i = 0; i < n ; i++) {
            arrayDivideByTwo[i] = numberFourDivideByTwo;
            numberFourDivideByTwo  += 4 / 2;
        }

        // Kalau index yang genap dijumlahkan
        // index ganjil dikurangi
        for (int i = 0; i < n ; i++) {
            if (i % 2 == 0){
                arraySum[i] = arrayDivideByTwo[i] + arrayThreeMinusOne[i];
            } else {
                arraySum[i] = arrayDivideByTwo[i] - arrayThreeMinusOne[i];
            }

        }

        for (int i = 0; i < n; i++) {
            System.out.print(arrayThreeMinusOne[i] +" ");
        }
        System.out.println("");
        for (int i = 0; i < n; i++) {
            System.out.print(arrayDivideByTwo[i] +" ");
        }

        System.out.println("\nHasil penjumlahan");
        System.out.println();
        for (int i = 0; i < n; i++) {
            System.out.print(arraySum[i] +" ");
        }
    }
}
