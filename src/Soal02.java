import java.util.Scanner;

public class Soal02 {
    // Todo : Belum selesai
    public static void main(String[] args) {
        System.out.println("Soal 02");
        Scanner input = new Scanner(System.in);
        System.out.println("Masukkan total Pulsa :");
        int totalPulsa = input.nextInt();
        totalPulsa -= 10000;
        // 0 - 10000
        double rangePertama = 0;
        double rangeKedua = 0;
        double rangeKeTiga = 0;
        double totalPoint = 0;
        if (totalPulsa >= 0 && totalPulsa <= 200000){
            if (totalPulsa >= 0){
                if (totalPulsa > 10_000){
                    int maxPoint = 10_000;
                    rangePertama = maxPoint/1000;
                }
                else {
                    rangePertama = totalPulsa/1000;
                }
            }
            if (totalPulsa >= 10_001){
                if (totalPulsa > 30_000){
                    int maxPoint = 20_000;
                    rangeKedua = maxPoint / 1000;
                }else {
                    rangeKedua = totalPulsa / 1000;
                }
            }
            if (totalPulsa >= 30_001 && totalPulsa <= 75_000){
                rangeKeTiga = (45_000 / 1000 ) * 2 ;
            }
            totalPoint = rangePertama + rangeKedua + rangeKeTiga;
        }

        System.out.println(rangePertama + " + " + rangeKedua + " + " + rangeKeTiga + " = " + totalPoint);
        System.out.println("Total Point : " + (totalPoint));
    }
}
