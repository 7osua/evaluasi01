import java.util.Arrays;
import java.util.Scanner;
import java.util.TreeMap;

public class Soal03 {
    public static void main(String[] args) {
        System.out.println("Soal 03");
        Scanner userInput = new Scanner(System.in);

        System.out.println("Input Record penjualan : ");
        String[] sellingRecords = userInput.nextLine().split(", ");
        Arrays.sort(sellingRecords);

        int[] jumlahBuah = new int[sellingRecords.length];
        String[] namaBuah = new String[sellingRecords.length];

        TreeMap<String, Integer> uniqueRecords = new TreeMap<String, Integer>();
        int keyIndexed = 0;

        for (int i = 0; i < sellingRecords.length; i++) {
            String[] dataSementara = sellingRecords[i].split(":");
            namaBuah[i] = dataSementara[0];
            jumlahBuah[i] += Integer.parseInt(dataSementara[1]);
        }

        for (int i = 1; i < sellingRecords.length; i++) {
            if(namaBuah[i - 1].equals(namaBuah[i])){
                jumlahBuah[i] += jumlahBuah[i-1];
            } else {
                continue;
            }
        }

        for (String buah: namaBuah) {
            uniqueRecords.put(buah,jumlahBuah[keyIndexed]);
            keyIndexed++;
        }

        // Urutkan berdasarkan penjualan buah terbanyak ?

        Arrays.sort(uniqueRecords.values().toArray(new Integer[0]));

        for (String key: uniqueRecords.keySet()) {
            System.out.println(key + " : " + uniqueRecords.get(key));
        }

    }
}
