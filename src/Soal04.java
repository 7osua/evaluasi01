import java.util.Locale;
import java.util.Scanner;

public class Soal04 {
    public static void main(String[] args) {
        System.out.println("Soal 04");
        Scanner userInput = new Scanner(System.in);

        System.out.print("JJ => Jam\t MM => Menit\t XX => AM atau PM\n");
        System.out.println("Masukkan format waktu JJ:MM XX\t :");
        String oldFormat = userInput.nextLine().toUpperCase(Locale.ROOT);
        int oldFormatLength = oldFormat.length();

        int hours = Integer.parseInt(oldFormat.substring(0,2));
        String minutes = oldFormat.substring(3,5);
        String status = "";
        String newFormat = "";

        if (oldFormatLength == 5) {
            if (hours >= 12) {
                newFormat += (hours - 12);
                if(!newFormat.equals("12")){
                    if (newFormat.length() < 2){
                        if (newFormat.equals("0") || hours <= 0){
                            newFormat = "12";
                        } else {
                            newFormat = "0" + newFormat;
                        }
                        newFormat += ":" + minutes + " PM";
                    } else {
                        newFormat += ":" + minutes + " PM";
                    }
                }

                if(newFormat.equals("12")){
                    newFormat = "12";
                    newFormat += ":" + minutes + " AM";
                }
            } else {
                if (hours < 10) {
                    if (hours == 0){
                        newFormat = "12";
                    } else {
                        newFormat = "0" + hours;
                    }
                    newFormat += ":" + minutes + " AM";
                } else {
                    newFormat += hours + newFormat;
                    newFormat += ":" + minutes + " AM";
                }
            }
        } else if(oldFormatLength == 8){
            status = oldFormat.substring(6).toUpperCase(Locale.ROOT);
            if(status.equals("PM")){
                newFormat += (hours + 12);
                if(newFormat.equals("24")){
                    newFormat = "00";
                    newFormat += ":" + minutes;
                } else {
                    newFormat += ":" + minutes;
                }
            }  else if(status.equals("AM")){
                if (hours < 10) {
                    newFormat += "0" + hours;
                    newFormat += ":" + minutes;
                } else {
                    newFormat += hours;
                    if (newFormat.length() == 2){
                        if(newFormat.equals("12")){
                            newFormat = "00" + ":" + minutes;
                        } else {
                            newFormat += ":" + minutes;
                        }
                    } else {
                        newFormat += "0";
                        newFormat += ":" + minutes;
                    }
                }
            }
        } else {
            System.out.println("Format tidak valid");
        }

        System.out.println(newFormat);

    }
}
