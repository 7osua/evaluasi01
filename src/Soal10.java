import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;

public class Soal10 {
    public static void main(String[] args) {
        System.out.println("Soal 10");

        // Pisahkan menjadi sebuah array atau untaian karakter
        // Pisahkan huruf vokal dan konsonan
        // urutkan huruf berdasarkan index huruf pada abjad

        Scanner userInput = new Scanner(System.in);

        System.out.println("Kalimat atau kata yang akan diproses\t:");
        String userWords = userInput.nextLine().toLowerCase(Locale.ROOT);


        char[] arrayOfChar = userWords.toCharArray();
        int charLength = arrayOfChar.length;
        char charValue;

        String vowelValues = "";
        String consonantValues = "";

        char arrayOfVowel[];
        char arrayOfConsonant[];

        int indexed = 0;

        for (int i = 0; i < charLength; i++) {
            charValue = arrayOfChar[i];
            indexed = userWords.charAt(i) - 'a';
            if(indexed <= 25 && indexed >= 0){
                if(charValue == 'a' || charValue == 'i' || charValue == 'u' || charValue == 'e' || charValue == 'o'){
                    vowelValues += Character.toString(charValue);
                } else {
                    consonantValues += Character.toString(charValue);
                }
            }
        }


        String resultSpecialCharacter = "";
        for (int j = 0; j < charLength; j++) {
            char aCharValue = arrayOfChar[j];
            String specialCharacters = "!@#$%^&*()-+";
            for (int k = 0; k < specialCharacters.length(); k++) {
                if(aCharValue == specialCharacters.toCharArray()[k]) {
                    if(specialCharacters.toCharArray()[k] == aCharValue) {
                        resultSpecialCharacter += specialCharacters.toCharArray()[k];
                    }
                }
            }
        }

        // yang selain vokal dan konsonan.
        // special karakter.

        arrayOfVowel = vowelValues.toCharArray();
        arrayOfConsonant = consonantValues.toCharArray();
        Arrays.sort(arrayOfVowel);
        Arrays.sort(arrayOfConsonant);
        vowelValues =  new String(arrayOfVowel) ;
        consonantValues = new String(arrayOfConsonant);

        System.out.println("Huruf Vokal : " + vowelValues);
        System.out.println("Huruf Konsonan : " + consonantValues);
        System.out.println("Spesial Karakter : " + resultSpecialCharacter);
    }
}
